package main

import (
	cwtchserver "cwtch.im/cwtch/server"
	"log"
)

const (
	serverConfigFile = "serverConfig.json"
)

func main() {
	serverConfig := cwtchserver.LoadConfig(serverConfigFile)

	server := new(cwtchserver.Server)
	log.Printf("starting cwtch server...")

	// TODO load params from .cwtch/server.conf or command line flag
	server.Run(serverConfig)
}
