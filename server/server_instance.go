package server

import (
	"cwtch.im/cwtch/protocol"
	"cwtch.im/cwtch/server/listen"
	"cwtch.im/cwtch/storage"
	"git.openprivacy.ca/openprivacy/libricochet-go/application"
	"git.openprivacy.ca/openprivacy/libricochet-go/channels"
)

// Instance encapsulates the Ricochet application.
type Instance struct {
	rai *application.ApplicationInstance
	ra  *application.RicochetApplication
	msi storage.MessageStoreInterface
}

// Init sets up a Server Instance
func (si *Instance) Init(rai *application.ApplicationInstance, ra *application.RicochetApplication, msi storage.MessageStoreInterface) {
	si.rai = rai
	si.ra = ra
	si.msi = msi
}

// HandleFetchRequest returns a list of all messages in the servers buffer
func (si *Instance) HandleFetchRequest() []*protocol.GroupMessage {
	return si.msi.FetchMessages()
}

// HandleGroupMessage takes in a group message and distributes it to all listening peers
func (si *Instance) HandleGroupMessage(gm *protocol.GroupMessage) {
	si.msi.AddMessage(*gm)
	go si.ra.Broadcast(func(rai *application.ApplicationInstance) {
		rai.Connection.Do(func() error {
			channel := rai.Connection.Channel("im.cwtch.server.listen", channels.Inbound)
			if channel != nil {
				cslc, ok := channel.Handler.(*listen.CwtchServerListenChannel)
				if ok {
					cslc.SendGroupMessage(gm)
				}
			}
			return nil
		})
	})
}
